# sisop-praktikum-modul-3-2023-mh-it03

Pengerjaan soal shift sistem operasi modul 3 oleh IT03

# Anggota

| Nama                            | NRP          |
| ------------------------------- | ------------ |
| Marcelinus Alvinanda Chrisantya | `5027221012` |
| George David Nebore             | `5027221043` |
| Angella Christie                | `5027221047` |

# Konten

- [Soal 1](#soal-1)

- [Soal 2](#soal-2)

- [Soal 3](#soal-3)

- [Soal 4](#soal-4)

# Soal 1
## Soal 1
### I. belajar.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int main() {
    // Menentukan ukuran matriks
    int nomor_kelompok = 3; 
    int baris1 = nomor_kelompok;
    int kolom1 = 2;
    int baris2 = 2;
    int kolom2 = nomor_kelompok;

    // Inisialisasi seed untuk angka acak
    srand(time(NULL));

    // Membuat matriks pertama dengan angka acak antara 1 hingga 4
    int matriks1[baris1][kolom1];
    for (int l = 0; l < baris1; l++) {
        for (int k = 0; k < kolom1; k++) {
            matriks1[l][k] = rand() % 4 + 1;
        }
    }
    // Membuat matriks kedua dengan angka acak antara 1 hingga 5
    int matriks2[baris2][kolom2];
    for (int l = 0; l < baris2; l++) {
        for (int k = 0; k < kolom2; k++) {
            matriks2[l][k] = rand() % 5 + 1;
        }
    }
    printf("Matriks Pertama:\n");
    for (int l = 0; l < baris1; l++) {
        for (int k = 0; k < kolom1; k++) {
            printf("%d ", matriks1[l][k]);
        }
        printf("\n");
    }
    printf("\nMatriks Kedua:\n");
    for (int l = 0; l < baris2; l++) {
        for (int k = 0; k < kolom2; k++) {
            printf("%d ", matriks2[l][k]);
        }
        printf("\n");
    }
    // Melakukan perkalian matriks dan menampilkan hasil awal
    if (kolom1 != baris2) {
        printf("\nPerkalian matriks tidak dapat dilakukan.\n");
    } else {
        int hasil[baris1][kolom2];
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                hasil[l][k] = 0;
                for (int t = 0; t < kolom1; t++) {
                    hasil[l][k] += matriks1[l][t] * matriks2[t][k];
                }
            }
        }
        printf("\nHasil Perkalian Matriks:\n");
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                printf("%d ", hasil[l][k]);
            }
            printf("\n");
        }
        // Mengurangi 1 pada hasil perkalian
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                hasil[l][k] -= 1;
            }
        }
        printf("\nHasil Perkalian Matriks - 1:\n");
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                printf("%d ", hasil[l][k]);
            }
            printf("\n");
        }
        // Membuat shared memory segment
        key_t key = ftok("belajar.c", 'C');
        if (key == -1) {
            perror("ftok");
            exit(1);
        }
        int shmid = shmget(key, sizeof(hasil), 0666 | IPC_CREAT);
        if (shmid == -1) {
            perror("shmget");
            exit(1);
        }
        // Melampirkan shared memory ke alamat program
        int (*sharedMatriks)[kolom2] = shmat(shmid, 0, 0);
        if (sharedMatriks == (int (*)[kolom2]) -1) {
            perror("shmat");
            exit(1);
        }
        // Menyalin hasil perkalian matriks ke shared memory
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                sharedMatriks[l][k] = hasil[l][k];
            }
        }
        // Detach shared memory dari alamat program
        shmdt(sharedMatriks);
    }
    return 0;
}
```
## Penjelasan Kode Program
1. Program memulai dengan mendefinisikan ukuran matriks pertama (baris1 dan kolom1) dan matriks kedua (baris2 dan kolom2).
2. Seed untuk angka acak diinisialisasikan dengan 1-4 untuk matriks 1 dan 1-5 untuk matriks 2.
3. Hasil perkalian matriks dihitung dan disimpan dalam matriks "hasil". Jika perkalian tidak memungkinkan, program memberikan pesan kesalahan. 
4. Hasil perkalian matriks dikurangi 1 dan hasilnya ditampilkan.
5. Program menggunakan shared memory untuk berbagi hasil perkalian matriks. Key shared memory dihasilkan menggunkan `ftok()`.
6. Shared memory segment dibuat dan di-attach ke alamat program.
7. Hasil perkalian matriks disalin ke shared memory. 
8. Shared memory di-detach dari alamat program. 

## yang.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#define MATRIKS_SIZE 3
// Struktur untuk mengirimkan data ke thread
struct DataThread {
   int baris;
   int kolom;
   int* Matriks;
};
// Fungsi untuk menghitung faktorial
unsigned long long faktorial(int n) {
   if (n == 0) return 1;
   return n * faktorial(n - 1);
}
// Fungsi thread untuk menghitung faktorial
void* menghitungfaktorial(void* arg) {
   struct DataThread* data = (struct DataThread*)arg;
   int baris = data->baris;
   int kolom = data->kolom;
   int* Matriks = data->Matriks;
   // Hitung faktorial dan simpan di dalam matriks
   Matriks[baris * MATRIKS_SIZE + kolom] = faktorial(Matriks[baris * MATRIKS_SIZE + kolom]);

   pthread_exit(NULL);
}
int main() {
   // Key yang digunakan harus sama dengan key yang digunakan dalam belajar.c
   key_t key = ftok("belajar.c", 'C');
   if (key == -1) {
       perror("ftok");
       exit(1);
   }
   int shmid = shmget(key, sizeof(int[MATRIKS_SIZE][MATRIKS_SIZE]), 0666);
   if (shmid == -1) {
       perror("shmget");
       exit(1);
   }
   // Attach shared memory ke alamat program
   int (*sharedMatriks)[MATRIKS_SIZE] = shmat(shmid, 0, 0);
   if (sharedMatriks == (int (*)[MATRIKS_SIZE]) -1) {
       perror("shmat");
       exit(1);
   }
   // Transpose matriks yang dibagikan melalui shared memory
   int transpose[MATRIKS_SIZE][MATRIKS_SIZE];
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           transpose[i][j] = sharedMatriks[j][i];
       }
   }
   printf("Matriks Transpose:\n");
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           printf("%d ", transpose[i][j]);
       }
       printf("\n");
   }
   // Buat thread untuk menghitung faktorial
   pthread_t threads[MATRIKS_SIZE][MATRIKS_SIZE];
   struct DataThread DataThread[MATRIKS_SIZE][MATRIKS_SIZE];
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           DataThread[i][j].baris = i;
           DataThread[i][j].kolom = j;
           DataThread[i][j].Matriks = (int*)transpose;

           pthread_create(&threads[i][j], NULL, calculatefaktorial, &DataThread[i][j]);
       }
   }
   // Tunggu hingga semua thread selesai
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           pthread_join(threads[i][j], NULL);
       }
   }
   printf("Matriks Hasil Faktorial:\n");
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           printf("%llu ", (unsigned long long)transpose[i][j]);
       }
       printf("\n");
   }
   // Detach shared memory dari alamat program
   shmdt(sharedMatriks);

   return 0;
}
```
## Penjelasan Kode Program 
1. Program memulai dengan mengamil shared memory yang telah dibuat oleh program belajar. menggunakan key yang sama.
2. Matriks yang dibagikan melalui shared memory di tranpose.
3. Program kemudian membuat thread-thread untuk menghitung faktorial untuk msetiap elemen matriks.
4. Setiap thread menerima data yang berisi informasi baris, kolom, dan matriks yang akan dihitung faktorialnya.
5. Dalam fungsi `menghitungfaktorial`, faktorial dari elemen matriks dihitung dan disimpan kembali ke matriks tersebut.
6. Setelah semua thread selesai menghitung faktorial, program menampilkan matriks hasil faktorial.
7. Shared memory di-detach dari alamat program.

## rajin.C
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long faktorial(int n) {
   if (n == 0) return 1;
   unsigned long long hasil = 1;
   for (int i = 1; i <= n; i++) {
       hasil *= i;
   }
   return hasil;
}
int main() {
   // Key yang digunakan harus sama dengan key yang digunakan dalam belajar.c
   key_t key = ftok("belajar.c", 'C');
   if (key == -1) {
       perror("ftok");
       exit(1);
   }
   int shmid = shmget(key, sizeof(int[3][3]), 0666);
   if (shmid == -1) {
       perror("shmget");
       exit(1);
   }
   // Attach shared memory ke alamat program
   int (*sharedMatriks)[3] = shmat(shmid, 0, 0);
   if (sharedMatriks == (int (*)[3]) -1) {
       perror("shmat");
       exit(1);
   }
   // Transpose matriks yang dibagikan melalui shared memory
   int transpose[3][3];
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           transpose[i][j] = sharedMatriks[j][i];
       }
   }
   printf("Matriks Transpose:\n");
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           printf("%d ", transpose[i][j]);
       }
       printf("\n");
   }
   // Hitung faktorial dalam format matriks
   unsigned long long hasilMatriks[3][3];
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           hasilMatriks[i][j] = faktorial(transpose[i][j]);
       }
   }
   printf("Matriks Hasil Faktorial tanpa thread:\n");
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           printf("%llu ", hasilMatriks[i][j]);
       }
       printf("\n");
   }
   // Detach shared memory dari alamat program
   shmdt(sharedMatriks);
   return 0;
}
```
## Penjelasan Kode Program 
1. Program memulai dengan mengambil shared memory yang telah dibuat oleh program "belajar.c" menggunakan kunci yang sama.
2. Matriks yang dibagikan melalui shared memory di-transpose, yang berarti baris dan kolomnya ditukar.
3. Program kemudian menghitung faktorial dari setiap elemen matriks transposed dan menyimpannya dalam matriks `hasilMatriks`.
4. Hasil perhitungan faktorial ditampilkan dalam format matriks.
5. Shared memory di-detach dari alamat program.


## Output 
## belajar.c 
![image](https://github.com/Angel0010/repo_pemweb/assets/131789727/f361a311-98db-495f-a3d2-7bfe441c6ee6)

## yang.c 
![image](https://github.com/Angel0010/repo_pemweb/assets/131789727/5ae3abaa-6904-4c38-863c-f23aacea44de)

## rajin.c 
![image](https://github.com/Angel0010/repo_pemweb/assets/131789727/6e0b80ac-df4e-40ff-80c5-ef4185626bda)

# Soal 2


8balondors.c

Berikut dibawah ini program dari 8balondors.c yang digunakan untuk mencari kata atau huruf pada suatu file litik.txt yang sudah diinputkan
```
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <string.h>
    #include <sys/wait.h>
    #include <ctype.h>
    #include <time.h>

    #define MAX_SIZE 100

    void writeToLog(char type, char *message)
    {
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t t;
        struct tm *tm_info;
        time(&t);
        tm_info = localtime(&t);

        fprintf(logFile, "[%02d/%02d/%02d %02d:%02d:%02d] [%c] %s\n",
                tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, type, message);

        fclose(logFile);
    }
    }

    void countFrequency(char type, char *input, char *fileName)
    {
    int frequency[MAX_SIZE] = {0};
    int len = strlen(input);

    for (int i = 0; i < len; i++)
    {
        frequency[input[i]]++;
    }

    for (int i = 0; i < MAX_SIZE; i++)
    {
        if (frequency[i] > 0)
        {
            char message[MAX_SIZE];
            if (type == 'k')
            {
                sprintf(message, "Kata '%c' muncul sebanyak %d kali dalam file '%s'", i, frequency[i], fileName);
            }
            else if (type == 'h')
            {
                sprintf(message, "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", i, frequency[i], fileName);
            }
            writeToLog(type == 'k' ? 'K' : 'H', message);
        }
    }
    }

    int main(int argc, char *argv[])
    {
    int fd1[2];
    int fd2[2];

    char fixed_str[] = " ";
    char input_str[100];
    char concat_str[100];
    pid_t p;

    if (pipe(fd1) == -1)
    {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }
    if (pipe(fd2) == -1)
    {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }

    scanf("%s", input_str);
    p = fork();

    if (p < 0)
    {
        fprintf(stderr, "fork Failed");
        return 1;
    }
    else if (p > 0)
    {
        close(fd1[0]);
        write(fd1[1], input_str, strlen(input_str) + 1);
        close(fd1[1]);
        wait(NULL);
        close(fd2[1]);
        read(fd2[0], concat_str, 100);
        printf("Concatenated string %s\n", concat_str);
        close(fd2[0]);

        // Write the processed string to thebeatles.txt
        FILE *outputFile = fopen("thebeatles.txt", "w");
        if (outputFile != NULL)
        {
            fprintf(outputFile, "%s", concat_str);
            fclose(outputFile);
        }
        else
        {
            fprintf(stderr, "Error writing to thebeatles.txt");
            return 1;
        }
    }
    else
    {
        close(fd1[1]);
        read(fd1[0], concat_str, 100);
        int k = strlen(concat_str);
        int i;
        for (i = 0; i < strlen(fixed_str); i++)
            concat_str[k++] = fixed_str[i];
        concat_str[k] = '\0';
        close(fd1[0]);
        close(fd2[0]);

        FILE *file = fopen("lirik.txt", "r");
        if (file == NULL)
        {
            fprintf(stderr, "Error opening file lirik.txt");
            return 1;
        }

        char buffer[MAX_SIZE];
        fscanf(file, "%s", buffer);
        fclose(file);

        if (strcmp(argv[1], "-kata") == 0)
        {
            countFrequency('k', buffer, "lirik.txt");
        }
        else if (strcmp(argv[1], "-huruf") == 0)
        {
            countFrequency('h', buffer, "lirik.txt");
        }

        close(fd2[0]);
        write(fd2[1], concat_str, strlen(concat_str) + 1);
        close(fd2[1]);
        exit(0);
    }

    return 0;
    }
```
    
diatas merupakan program untuk mencari kata dan huruf pada suatu file.
```
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <string.h>
    #include <sys/wait.h>
    #include <ctype.h>
    #include <time.h>
```
Baris 1-8: Mendeklarasikan berbagai header file yang akan digunakan dalam program, seperti untuk operasi input/output, operasi sistem, manipulasi string, dan manipulasi waktu.
```
    #define MAX_SIZE 100
```
Baris 10: Mendefinisikan konstanta MAX_SIZE dengan nilai 100.
```
    void writeToLog(char type, char *message)
    {
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t t;
        struct tm *tm_info;
        time(&t);
        tm_info = localtime(&t);

        fprintf(logFile, "[%02d/%02d/%02d %02d:%02d:%02d] [%c] %s\n",
                tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
                tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, type, message);

        fclose(logFile);
    }
    }
```

Baris 12-32: Mendefinisikan fungsi writeToLog untuk menulis log. Menerima tipe ('k' atau 'h') dan pesan sebagai argumen. Membuka file "frekuensi.log" untuk ditulis, dan menulis pesan log dengan format timestamp.

```
    void countFrequency(char type, char *input, char *fileName)
    {
    int frequency[MAX_SIZE] = {0};
    int len = strlen(input);

    for (int i = 0; i < len; i++)
    {
        frequency[input[i]]++;
    }

    for (int i = 0; i < MAX_SIZE; i++)
    {
        if (frequency[i] > 0)
        {
            char message[MAX_SIZE];
            if (type == 'k')
            {
                sprintf(message, "Kata '%c' muncul sebanyak %d kali dalam file '%s'", i, frequency[i], fileName);
            }
            else if (type == 'h')
            {
                sprintf(message, "Huruf '%c' muncul sebanyak %d kali dalam file '%s'", i, frequency[i], fileName);
            }
            writeToLog(type == 'k' ? 'K' : 'H', message);
        }
    }
    }
```
Baris 34-62: Mendefinisikan fungsi countFrequency untuk menghitung frekuensi karakter pada string input. Menerima tipe ('k' atau 'h'), string input, dan nama file sebagai argumen. Menyimpan frekuensi karakter yang lebih besar dari 0 dalam array, dan menulis log untuk setiap karakter yang muncul lebih dari 0 kali.

```
    int main(int argc, char *argv[])
    {
    // ... (Deklarasi variabel dan array)

memulai fungsi main

        int fd1[2];
    int fd2[2];

    char fixed_str[] = " ";
    char input_str[100];
    char concat_str[100];
    pid_t p;

    if (pipe(fd1) == -1)
    {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }
    if (pipe(fd2) == -1)
    {
        fprintf(stderr, "Pipe Failed");
        return 1;
    }
```
Baris 68-79: Mendeklarasikan variabel dan array, termasuk dua array pipe (fd1 dan fd2). Mengecek kegagalan pembuatan pipe dan memberikan pesan kesalahan jika terjadi.

```
        scanf("%s", input_str);
```
Baris 81: Membaca string dari input pengguna.

```
        p = fork();

    if (p < 0)
    {
        fprintf(stderr, "fork Failed");
        return 1;
    }
    else if (p > 0)
    {
        // ... (Proses Induk)
```
Baris 83-90: Membuat proses anak menggunakan fork. Jika fork gagal, mencetak pesan kesalahan. Jika proses induk, melanjutkan ke bagian proses induk.

```
            close(fd1[0]);
        write(fd1[1], input_str, strlen(input_str) + 1);
        close(fd1[1]);
        wait(NULL);
```
Baris 92-97: Menutup ujung pembaca pipe pertama, menulis string input ke dalam pipe pertama, menutup ujung penulis pipe pertama, dan menunggu proses anak selesai.

```
            close(fd2[1]);
        read(fd2[0], concat_str, 100);
        printf("Concatenated string %s\n", concat_str);
        close(fd2[0]);
```
Baris 99-105: Menutup ujung penulis pipe kedua, membaca string hasil konkatenasi dari pipe kedua, mencetak string tersebut, dan menutup ujung pembaca pipe kedua.

```
            FILE *outputFile = fopen("thebeatles.txt", "w");
        if (outputFile != NULL)
        {
            fprintf(outputFile, "%s", concat_str);
            fclose(outputFile);
        }
        else
        {
            fprintf(stderr, "Error writing to thebeatles.txt");
            return 1;
        }
```
Baris 107-117: Membuka file "thebeatles.txt" untuk ditulis, mencetak string hasil konkatenasi ke dalam file tersebut, dan menutup file. Jika gagal, mencetak pesan kesalahan.

```
    }
    else
    {
        // ... (Proses Anak)
    }

    return 0;
    }
```
Baris 119-125: Jika proses anak, melanjutkan ke bagian proses anak.

```
        close(fd1[1]);
        read(fd1[0], concat_str, 100);
        int k = strlen(concat_str);
        int i;
        for (i = 0; i < strlen(fixed_str); i++)
            concat_str[k++] = fixed_str[i];
        concat_str[k] = '\0';
        close(fd1[0]);
        close(fd2[0]);
```
Baris 127-136: Menutup ujung penulis pipe pertama, membaca string dari pipe pertama, menambahkan string dengan string tetap " ", dan menutup ujung pembaca pipe pertama dan ujung pembaca pipe kedua.

```
        FILE *file = fopen("lirik.txt", "r");
        if (file == NULL)
        {
            fprintf(stderr, "Error opening file lirik.txt");
            return 1;
        }

        char buffer[MAX_SIZE];
        fscanf(file, "%s", buffer);
        fclose(file);
```
Baris 138-146: Membuka file "lirik.txt" untuk dibaca, membaca kata pertama dari file tersebut, dan menutup file. Jika gagal membuka file, mencetak pesan kesalahan.

```
            if (strcmp(argv[1], "-kata") == 0)
        {
            countFrequency('k', buffer, "lirik.txt");
        }
        else if (strcmp(argv[1], "-huruf") == 0)
        {
            countFrequency('h', buffer, "lirik.txt");
        }
```
Baris 148-154: Memeriksa argumen dari command line. Jika argumen adalah "-kata", memanggil fungsi countFrequency untuk menghitung frekuensi kata dalam file "lirik.txt". Jika argumen adalah "-huruf", memanggil fungsi countFrequency untuk menghitung frekuensi huruf.

```
        close(fd2[0]);
        write(fd2[1], concat_str, strlen(concat_str) + 1);
        close(fd2[1]);
        exit(0);
    }
```
Baris 156-163: Menutup ujung pembaca pipe kedua, menulis string hasil proses ke dalam pipe kedua, menutup ujung penulis pipe kedua, dan keluar dari proses anak.

```
    return 0;
}
```
membalikan fungsi main ke nilai 0.



# Soal 3

Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 
1. Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut

```
└── soal3
	├── users
	├── receiver.c
	└── sender.c
```

2. Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut : 

```
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
….
```

3. Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.
4. Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:

```
└── soal3
       ├── Receiver
       ├── Sender
       ├── receiver.c
       └── sender.c
```



5. Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.
6. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".
```
Catatan:
- Dilarang untuk overwrite file users secara permanen
- Ketika proses autentikasi gagal program receiver akan mengirim status - Authentication failed dan program langsung keluar
- Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
- Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.
```
## Program

Berikut kedua program sender.c dan receiver.c :

## sender.c
```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define MAX 100

int status;

// structure for message queue
struct message {
    long msg_type;
    char msg_text[256];
};

void downloadFile() {
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "users.zip", "https://drive.google.com/u/0/uc?id=1CrERpikZwuxgAwDvrhRnB2VyAYtT2SIf&export=download", NULL );
        exit(EXIT_SUCCESS);
    }  waitpid(wget, &status, 0);
}

void extractZip(){
    pid_t unzip = fork();
    if (unzip == 0) {
        execl("/usr/bin/unzip", "unzip", "-o", "users.zip",  NULL);
        perror("execl");
        exit(EXIT_FAILURE); 
    }
    else if (unzip > 0) {

        waitpid(unzip, &status, 0); 
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            remove("users.zip"); 
        } else {
            printf("Unzip failed.\n");
        }
    }
    else {
        perror("fork");
    }
    waitpid(unzip, &status, 0);
}

void makeDirectory() {
    pid_t mkdirSender, mkdirReceiver;

    mkdirSender = fork();

    if (mkdirSender == 0) {
        int create = mkdir("Sender", 0777); 
        if (create != 0) {
            perror("Error creating directory");
        } else {
            printf("Directory Sender created successfully.\n");
        }
        exit(EXIT_SUCCESS);
    } else if (mkdirSender > 0) {
        mkdirReceiver = fork();
        if (mkdirReceiver == 0) {
            int create = mkdir("Receiver", 0777); 
            if (create != 0) {
                perror("Error creating directory");
            } else {
                printf("Directory Receiver created successfully.\n");
            }
            exit(EXIT_SUCCESS);
        } else if (mkdirReceiver < 0) {
            perror("Error forking child process 2");
        }
    } else {
        perror("Error forking child process 1");
    }
}

int main() {
    makeDirectory();
    downloadFile();
    extractZip();

    int msgid;
    struct message msg;

	// ftok to generate unique key
    key_t key = ftok("sender", 'R');
    if (key == -1) {
        perror("ftok");
        exit(1);
    }

	// msgget creates a message queue
	// and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }
 
    while(1){
        
    printf("Masukkan pesan Anda: ");
    fgets(msg.msg_text, MAX, stdin);
    msg.msg_text[strcspn(msg.msg_text, "\n")] = 0;  

    msg.msg_type = 1;

	// msgsnd to send message
    if (msgsnd(msgid, &msg, strlen(msg.msg_text) + 1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

	// display the message
    printf("Pesan telah terkirim ke receiver.\n");
    }
    return 0;
}
```

## receiver.c

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

struct message {
    long msg_type;
    char msg_text[256];
};

// Fungsi decrypt base64
char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}

int main() {
    int msgid;
    struct message msg;
    int authenticated = 0;

    while(1){
       key_t key = ftok("sender", 'R');
        if (key == -1) {
            perror("ftok");
            exit(1);
        }

        msgid = msgget(key, 0666 | IPC_CREAT);
        if (msgid == -1) {
            perror("msgget");
            exit(1);
        }

        if (msgrcv(msgid, &msg, sizeof(msg.msg_text), 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        printf("Pesan yang diterima: %s\n", msg.msg_text);

        if (strcmp(msg.msg_text, "CREDS") == 0) {
            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];
            while (fgets(line, sizeof(line), file) != NULL) {
                char *username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);
                printf("Username: %s, Password: %s\n", username, decoded_password);
                free(decoded_password);
            }
            fclose(file);

        } else if (strncmp(msg.msg_text, "AUTH:", 5) == 0) {
            char *username = strtok(msg.msg_text + 6, " ");
            char *password = strtok(NULL, " ");

            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];

            while (fgets(line, sizeof(line), file) != NULL) {
                char *file_username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);

                if (strcmp(username, file_username) == 0 && strcmp(password, decoded_password) == 0) {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                free(decoded_password);
            }
            fclose(file);

        } else if (authenticated == 1 && strncmp(msg.msg_text, "TRANSFER:", 9) == 0) {
            char *filename = strtok(msg.msg_text + 10, " ");
            char sender_filepath[100];
            char receiver_filepath[100];

            sprintf(sender_filepath, "Sender/%s", filename);
            sprintf(receiver_filepath, "Receiver/%s", filename);

            // cek keberadaan file
            if (access(receiver_filepath, F_OK) != -1) {
                printf("File '%s' already exists in Receiver directory. Skipping transfer.\n", filename);
                break;
            } else {
                if (rename(sender_filepath, receiver_filepath) != 0) {
                    perror("Gagal memindahkan file");
                    exit(1);
                }

                FILE *file = fopen(receiver_filepath, "rb");
                fseek(file, 0, SEEK_END);
                long size = ftell(file);
                fclose(file);

                float size_kb = size / 1024.0;

                printf("File '%s' berhasil dipindahkan dari Sender ke Receiver. Ukuran: %.2f KB\n", filename, size_kb);
            }
                } else if (!authenticated) {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    break;
                } else {
            printf("UNKNOWN COMMAND: %s\n", msg.msg_text);
        }
    }
    return 0;
}
```

## Analisa

Berikut analisa dari setiap fungsi pada kedua program di atas:

## sender.c

Berikut adalah analisa setiap baris kode dalam program sender.c:

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define MAX 100

int status;
```
- Bagian ini adalah untuk memuat pustaka-pustaka standar C yang diperlukan.
- `#define MAX 100` mendefinisikan konstanta `MAX` dengan nilai 100.
- `int status;` mendeklarasikan variabel `status` bertipe integer. Variabel ini digunakan untuk menyimpan status keluar dari proses anak.

```c
struct message {
    long msg_type;
    char msg_text[256];
};
```
- Mendefinisikan sebuah struktur bernama `message` yang memiliki dua anggota: `msg_type` bertipe long dan `msg_text` yang merupakan array karakter dengan panjang maksimum 256.

```c
void downloadFile() {
    // ...
}
```
- Mendefinisikan fungsi `downloadFile` yang berfungsi untuk mengunduh file menggunakan `wget`.

```c
void extractZip(){
    // ...
}
```
- Mendefinisikan fungsi `extractZip` yang berfungsi untuk mengekstrak file zip menggunakan `unzip`.

```c
void makeDirectory() {
    // ...
}
```
- Mendefinisikan fungsi `makeDirectory` yang membuat dua direktori bernama "Sender" dan "Receiver".

```c
int main() {
    // ...
}
```
- Mendefinisikan fungsi `main` yang merupakan titik masuk dari program.

```c
makeDirectory();
downloadFile();
extractZip();
```
- Memanggil fungsi-fungsi yang sudah didefinisikan sebelumnya, yaitu `makeDirectory()`, `downloadFile()`, dan `extractZip()`.

```c
int msgid;
struct message msg;

key_t key = ftok("sender", 'R');
if (key == -1) {
    perror("ftok");
    exit(1);
}
```
- Mendeklarasikan variabel `msgid` dan struktur `msg`.
- `key_t key` digunakan untuk menghasilkan kunci unik untuk antrean pesan menggunakan `ftok()`. `ftok()` menggunakan nama file "sender" dan karakter 'R' untuk menghasilkan kunci ini.

```c
msgid = msgget(key, 0666 | IPC_CREAT);
if (msgid == -1) {
    perror("msgget");
    exit(1);
}
```
- `msgid` diberi nilai yang dikembalikan oleh `msgget()`. Fungsi ini menciptakan antrean pesan (atau mendapatkan yang sudah ada) dan mengembalikan pengidentifikasi (`msgid`) yang terkait dengannya. 
- `0666 | IPC_CREAT` menetapkan izin untuk antrean pesan.

```c
while(1){
    // ...
}
```
- Memulai loop tak terbatas.

```c
printf("Masukkan pesan Anda: ");
fgets(msg.msg_text, MAX, stdin);
msg.msg_text[strcspn(msg.msg_text, "\n")] = 0;
```
- Mencetak pesan "Masukkan pesan Anda: ".
- Menggunakan `fgets()` untuk mendapatkan baris input dari pengguna dan menyimpannya dalam `msg.msg_text`.
- `strcspn(msg.msg_text, "\n")` mencari posisi karakter baris baru dalam `msg.msg_text` dan `msg.msg_text[strcspn(msg.msg_text, "\n")] = 0;` menggantikannya dengan terminasi null, efektif menghilangkan karakter baris baru di akhir.

```c
msg.msg_type = 1;
```
- Menetapkan nilai `msg_type` dalam struktur `msg` menjadi 1.

```c
if (msgsnd(msgid, &msg, strlen(msg.msg_text) + 1, 0) == -1) {
    perror("msgsnd");
    exit(1);
}
```
- Menggunakan `msgsnd()` untuk mengirim pesan ke antrean pesan. Fungsi ini mengambil pengidentifikasi antrean pesan (`msgid`), pointer ke struktur `msg`, ukuran pesan (`strlen(msg.msg_text) + 1` untuk memasukkan terminasi null), dan bendera (0 dalam hal ini).

```c
printf("Pesan telah terkirim ke receiver.\n");
```
- Mencetak pesan yang mengindikasikan bahwa pesan telah dikirim ke penerima.

```c
return 0;
```
- Menandakan terminasi program yang berhasil.

## receiver.c

Berikut penjelasan pada program receiver.c:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

struct message {
    long msg_type;
    char msg_text[256];
};
```

- Kode di atas memulai dengan mendeklarasikan beberapa pustaka (libraries) dan mendefinisikan sebuah struktur `message`. 
- Pustaka yang digunakan mencakup standar C seperti `stdio.h`, `stdlib.h`, dan `string.h`. 
- Pustaka lain termasuk pustaka sistem (`sys/msg.h`, `sys/types.h`, `sys/stat.h`), serta pustaka OpenSSL (`openssl/bio.h`, `openssl/evp.h`). 
- Struktur `message` memiliki dua anggota: `msg_type` yang bertipe `long` dan `msg_text` yang merupakan array karakter dengan panjang maksimum 256.

```c
char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}
```

- Fungsi `decryptBase64` mengambil input berupa string yang dienkripsi menggunakan algoritma base64 dan mengembalikan string hasil dekripsi.
- Fungsi ini menggunakan pustaka OpenSSL untuk melakukan dekripsi base64.
- Proses dekripsi melibatkan beberapa operasi menggunakan objek BIO (BIO_new, BIO_set_flags, BIO_new_mem_buf, dll).
- Fungsi mengembalikan string hasil dekripsi atau NULL jika terjadi kegagalan alokasi memori.

```c
int main() {
    int msgid;
    struct message msg;
    int authenticated = 0;
```

- Fungsi `main` dimulai. Variabel `msgid` akan menyimpan ID dari antrean pesan. 
- Struktur `message msg` akan digunakan untuk menyimpan pesan yang diterima.
- `authenticated` adalah variabel yang digunakan untuk menandai apakah pengguna telah terautentikasi atau belum.

```c
    while(1){
       key_t key = ftok("sender", 'R');
        if (key == -1) {
            perror("ftok");
            exit(1);
        }

        msgid = msgget(key, 0666 | IPC_CREAT);
        if (msgid == -1) {
            perror("msgget");
            exit(1);
        }

        if (msgrcv(msgid, &msg, sizeof(msg.msg_text), 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        printf("Pesan yang diterima: %s\n", msg.msg_text);
```

- Program memasuki loop tak terbatas untuk terus menerima pesan. 
- `key_t key` adalah variabel yang digunakan untuk menyimpan kunci yang akan digunakan untuk antrean pesan.
- `ftok` menghasilkan kunci unik berdasarkan file "sender" dan karakter 'R'.
- `msgid` diberi nilai dari hasil panggilan `msgget()`, yang membuat atau mendapatkan antrean pesan dan mengembalikan ID yang terkait.
- `msgrcv()` digunakan untuk menerima pesan dari antrean pesan. Jika terjadi kesalahan, program akan keluar.

```c
        if (strcmp(msg.msg_text, "CREDS") == 0) {
            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];
            while (fgets(line, sizeof(line), file) != NULL) {
                char *username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);
                printf("Username: %s, Password: %s\n", username, decoded_password);
                free(decoded_password);
            }
            fclose(file);

        }
```

Kode ini akan membuka dan membaca file users.txt ketika inputan dari sender adalah CREDS. Kemudian program akan menyimpan hasil baca tadi ke dalam pointer FILE, dan didecrypt oleh fungsi decryptBase64 tadi kemudian hasil akhirnya akan ditampilkan.

```
else if (strncmp(msg.msg_text, "AUTH:", 5) == 0) {
            char *username = strtok(msg.msg_text + 6, " ");
            char *password = strtok(NULL, " ");

            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];

            while (fgets(line, sizeof(line), file) != NULL) {
                char *file_username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);

                if (strcmp(username, file_username) == 0 && strcmp(password, decoded_password) == 0) {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                free(decoded_password);
            }
            fclose(file);

        }
```
Kode di atas akan membuka file users.txt ketika pesan yang masuk adalah "TRANSFER: username password". Kemudian username dan password akan disimpan pada variabel, dan selanjutnya akan dilakukan
penyocokkan username dan password pada users.txt yang sudah didecrypt dengan inputan tadi. Apabila benar akan ditampilkan output Authentication successful dan value dari authenticated berubah menjadi 1, menandakan bahwa user sudah login.

```
else if (authenticated == 1 && strncmp(msg.msg_text, "TRANSFER:", 9) == 0) {
            char *filename = strtok(msg.msg_text + 10, " ");
            char sender_filepath[100];
            char receiver_filepath[100];

            sprintf(sender_filepath, "Sender/%s", filename);
            sprintf(receiver_filepath, "Receiver/%s", filename);

            // cek keberadaan file
            if (access(receiver_filepath, F_OK) != -1) {
                printf("File '%s' already exists in Receiver directory. Skipping transfer.\n", filename);
                break;
            } else {
                if (rename(sender_filepath, receiver_filepath) != 0) {
                    perror("Gagal memindahkan file");
                    exit(1);
                }

                FILE *file = fopen(receiver_filepath, "rb");
                fseek(file, 0, SEEK_END);
                long size = ftell(file);
                fclose(file);

                float size_kb = size / 1024.0;

                printf("File '%s' berhasil dipindahkan dari Sender ke Receiver. Ukuran: %.2f KB\n", filename, size_kb);
            }
                } else if (!authenticated) {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    break;
                } else {
            printf("UNKNOWN COMMAND: %s\n", msg.msg_text);
        }
```

Kode ini adalah bagian dari kondisi `else if (authenticated == 1 && strncmp(msg.msg_text, "TRANSFER:", 9) == 0) { ... }`, yang berarti jika pengguna telah terautentikasi dan pesan yang diterima dimulai dengan "TRANSFER:", maka akan dilakukan operasi berikut:

```c
char *filename = strtok(msg.msg_text + 10, " ");
```
- Kode di atas menggunakan `strtok()` untuk memisahkan string pesan `msg.msg_text` dimulai dari karakter ke-10 (karena "TRANSFER:" memiliki sembilan karakter) dengan delimiter spasi (" "). 
- Hasilnya, variabel `filename` akan menyimpan nama file yang akan ditransfer.

```c
char sender_filepath[100];
char receiver_filepath[100];

sprintf(sender_filepath, "Sender/%s", filename);
sprintf(receiver_filepath, "Receiver/%s", filename);
```
- Kode ini membangun path (alamat file) untuk pengirim dan penerima dengan menggunakan `sprintf()`.
- Variabel `sender_filepath` akan berisi path dari direktori "Sender/" ditambah dengan nama file.
- Variabel `receiver_filepath` akan berisi path dari direktori "Receiver/" ditambah dengan nama file yang sama.

```c
// cek keberadaan file
if (access(receiver_filepath, F_OK) != -1) {
    printf("File '%s' already exists in Receiver directory. Skipping transfer.\n", filename);
    break;
} else {
    // ...
}
```
- Kode ini memeriksa apakah file dengan nama yang sama sudah ada di direktori "Receiver/".
- Jika file sudah ada, maka program mencetak pesan bahwa file sudah ada dan proses transfer dilewati.

```c
if (rename(sender_filepath, receiver_filepath) != 0) {
    perror("Gagal memindahkan file");
    exit(1);
}

FILE *file = fopen(receiver_filepath, "rb");
fseek(file, 0, SEEK_END);
long size = ftell(file);
fclose(file);

float size_kb = size / 1024.0;

printf("File '%s' berhasil dipindahkan dari Sender ke Receiver. Ukuran: %.2f KB\n", filename, size_kb);
```
- Jika file belum ada di direktori "Receiver/", maka program akan mencoba untuk memindahkan file dari "Sender/" ke "Receiver/" menggunakan `rename()`.
- Jika pemindahan berhasil, program membuka file yang baru dipindahkan dan menghitung ukurannya dalam kilobyte.
- Setelah itu, file ditutup kembali.
- Ukuran file dalam kilobyte dihitung dan dicetak bersama dengan pesan bahwa file berhasil dipindahkan.

```c
} else if (!authenticated) {
    printf("Authentication failed\n");
    authenticated = 0;
    msgctl(msgid, IPC_RMID, NULL);
    break;
} else {
    printf("UNKNOWN COMMAND: %s\n", msg.msg_text);
}
```
- Jika pengguna belum terautentikasi (`authenticated` sama dengan 0), program mencetak pesan bahwa autentikasi gagal dan kemudian menghentikan antrean pesan menggunakan `msgctl()` dan keluar dari loop menggunakan `break`.
- Jika kondisi di atas tidak terpenuhi, program mencetak pesan bahwa perintah tidak dikenali.

Secara keseluruhan, kode ini melakukan proses transfer file dari "Sender/" ke "Receiver/" jika file tersebut belum ada di "Receiver/". Jika pengguna belum terautentikasi, program mencetak pesan autentikasi gagal dan menghentikan antrean pesan. Jika perintah tidak dikenali, program mencetak pesan bahwa perintah tidak dikenali.

## Output

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/a1a27fd1-baeb-43a6-ac54-dae39adfa0ac)

Tampilan tree dan awal dari program receiver dan sender.

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/ae096ef9-ddeb-48d6-9c71-af11a345eab8)

Ketika CREDS masuk sebagai input di sender.c.

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/94d2dc00-b4f6-44b1-a738-1e5ea3a26df4)

Ketika dimasukkan Auth yang sesuai.

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/43f26983-01f7-4a19-ac1d-5723d4732d5b)

Ketika Transfer tapi belum AUTH, program receiver akan keluar.

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/5dc39e1b-17e9-4237-bcb6-c274d9b1f92d)

Ketika input TRANSFER: filename. Dan sudah melakukan AUTH.

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/2c72a85a-7fbe-45ad-975e-a6257c77c69c)

Ketika Transfer tapi sudah ada file dengan nama yang sama di folder Receiver.

# Soal 4

Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:
- Client dan server terhubung melalui socket. 
- Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  
- karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja. 

## Program 

### client.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    char message[1024] = {0}; // Menambah buffer untuk pesan dari pengguna

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    // Meminta pengguna untuk memasukkan pesan
    printf("Enter a message: ");
    fgets(message, sizeof(message), stdin);

    // Mengirim pesan ke server
    send(sock , message , strlen(message) , 0 );
    printf("Message sent\n");



    return 0;
}

```

### server.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

fd_set readfds; // Set file descriptor untuk pemantauan

#define PORT 8080
#define MAX_CLIENTS 5

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
    int client_sockets[MAX_CLIENTS] = {0}; // Menyimpan file descriptor untuk klien
    
    // Membuat soket server
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    
    // Set opsi soket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    
    // Mengeksekusi bind
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Memulai mendengarkan
    if (listen(server_fd, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    int num_clients = 0;

    while (1) {
    FD_ZERO(&readfds); // Kosongkan set file descriptor
    FD_SET(server_fd, &readfds); // Tambahkan server socket ke set

    int max_fd = server_fd;

    // Tambahkan klien socket yang valid ke dalam set
    for (int i = 0; i < num_clients; i++) {
        int client_socket = client_sockets[i];
        if (client_socket > 0) {
            FD_SET(client_socket, &readfds);
            if (client_socket > max_fd) {
                max_fd = client_socket;
            }
        }
    }

    // Gunakan select untuk memantau koneksi
    int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);

    if (activity < 0) {
        perror("select");
        exit(EXIT_FAILURE);
    }

    // Cek server socket untuk koneksi baru
    if (FD_ISSET(server_fd, &readfds)) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // Tambahkan soket klien ke daftar
        if (num_clients < MAX_CLIENTS) {
            client_sockets[num_clients++] = new_socket;
            printf("Client %d connected\n", num_clients);
        } else {
            printf("Maximum number of clients reached. Closing new connection.\n");
            close(new_socket);
            exit(1);
        }
    }

    // Cek klien socket untuk data masuk
    for (int i = 0; i < num_clients; i++) {
        int client_socket = client_sockets[i];
        if (FD_ISSET(client_socket, &readfds)) {
            valread = read(client_socket, buffer, sizeof(buffer));
            if (valread <= 0) {
                // Koneksi terputus
                printf("Client %d disconnected\n", i + 1);
                close(client_socket);
                num_clients--;
                client_sockets[i] = 0;
            } else {
                printf("Message from client %d: %s\n", i + 1, buffer);
                memset(buffer, 0, sizeof(buffer)); // Reset buffer
            }
        }
    }
}

    return 0;
}

```

## Analisa

Berikut analisa dari setiap fungsi pada kedua program di atas:

## cleaner.c

```
int main(int argc, char const *argv[]):
```

Fungsi utama dari program.
Menerima dua argumen: argc yang merupakan jumlah argumen baris perintah, dan argv yang merupakan array dari argumen tersebut.
socket:

Fungsi untuk membuat socket.
Parameter: 
```
AF_INET (protokol IPv4), SOCK_STREAM
``` 
(tipe koneksi TCP), dan 0 (default protocol, umumnya 0).
Mengembalikan file descriptor dari socket yang baru dibuat.
memset:

Fungsi untuk mengisi blok memori dengan nilai tertentu.
Parameter: Alamat memori 
```
(&serv_addr), nilai untuk diisi ('0'), dan ukuran dalam byte (sizeof(serv_addr)).
inet_pton:
```

Fungsi untuk mengonversi alamat IP dari format string ke format biner.
Parameter: 
```
AF_INET (IPv4), alamat IP dalam format string ("127.0.0.1"), dan alamat untuk menyimpan hasil konversi (&serv_addr.sin_addr).
connect:
```

Fungsi untuk menghubungkan socket ke alamat server.
Parameter: 
```
File descriptor dari socket (sock), alamat server (&serv_addr), dan ukuran alamat server dalam byte (sizeof(serv_addr)).
printf("Enter a message: "):
```

Menampilkan pesan kepada pengguna untuk memasukkan pesan.
```
fgets(message, sizeof(message), stdin):
```

Membaca input dari pengguna (dalam hal ini, pesan) dari standar input (keyboard) dan menyimpannya di message.
```
send(sock , message , strlen(message) , 0 ):
```
Mengirim data melalui koneksi socket.
Parameter: 
```
File descriptor dari socket (sock), data yang akan dikirim (message), panjang data (strlen(message)), dan flag tambahan (0).
printf("Message sent\n"):
```

Menampilkan pesan bahwa pesan telah terkirim.
```
return 0:
```

Mengembalikan 0 sebagai tanda bahwa program telah berakhir dengan sukses.

## server.c

Program ini adalah implementasi dari server socket dalam bahasa C. Berikut adalah penjelasan dari setiap bagian kode:

1. **Pustaka yang Digunakan**:
   ```c
   #include <stdio.h>
   #include <sys/socket.h>
   #include <stdlib.h>
   #include <netinet/in.h>
   #include <string.h>
   #include <unistd.h>
   #include <sys/select.h>
   #include <sys/time.h>
   ```
   - Program menggunakan beberapa pustaka standar C dan pustaka terkait socket dan I/O.

2. **Deklarasi Variabel dan Struktur**:
   ```c
   fd_set readfds; // Set file descriptor untuk pemantauan
   #define PORT 8080
   #define MAX_CLIENTS 5
   ```
   - `fd_set readfds`: Digunakan untuk memantau file descriptor (socket).
   - `PORT`: Menyimpan nomor port yang akan digunakan.
   - `MAX_CLIENTS`: Menyimpan jumlah maksimum klien yang dapat terhubung ke server.

3. **Deklarasi Variabel**:
   ```c
   int server_fd, new_socket, valread;
   struct sockaddr_in address;
   int opt = 1;
   int addrlen = sizeof(address);
   char buffer[1024] = {0};
   char *hello = "Hello from server";
   int client_sockets[MAX_CLIENTS] = {0}; // Menyimpan file descriptor untuk klien
   ```
   - `server_fd`: File descriptor dari socket server.
   - `new_socket`: File descriptor dari koneksi baru yang diterima.
   - `valread`: Variabel untuk menyimpan jumlah byte yang dibaca dari koneksi.
   - `address`: Struktur untuk menyimpan informasi alamat socket.
   - `opt`: Opsi untuk socket (digunakan untuk setsockopt).
   - `addrlen`: Ukuran dari struktur alamat.
   - `buffer`: Buffer untuk membaca dan menyimpan data dari koneksi.
   - `hello`: Pesan yang akan dikirimkan ke klien saat pertama kali terhubung.
   - `client_sockets`: Array untuk menyimpan file descriptor dari klien-klien yang terhubung.

4. **Membuat Soket Server**:
   ```c
   if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
       perror("socket failed");
       exit(EXIT_FAILURE);
   }
   ```
   - Membuat socket server menggunakan protokol IPv4 dan tipe koneksi TCP.

5. **Set Opsi Soket**:
   ```c
   if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
       perror("setsockopt");
       exit(EXIT_FAILURE);
   }
   ```
   - Mengatur opsi soket agar dapat menggunakan alamat dan port yang sama secara berulang-ulang.

6. **Mengatur Informasi Alamat**:
   ```c
   address.sin_family = AF_INET;
   address.sin_addr.s_addr = INADDR_ANY;
   address.sin_port = htons( PORT );
   ```
   - Mengisi struktur alamat dengan informasi yang diperlukan.

7. **Eksekusi Bind**:
   ```c
   if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
       perror("bind failed");
       exit(EXIT_FAILURE);
   }
   ```
   - Mengaitkan alamat dan port yang telah ditentukan dengan socket.

8. **Memulai Mendengarkan**:
   ```c
   if (listen(server_fd, 5) < 0) {
       perror("listen");
       exit(EXIT_FAILURE);
   }
   ```
   - Memulai proses mendengarkan koneksi dari klien. Antrian maksimum untuk koneksi pending adalah 5.

9. **Loop Utama**:
   ```c
   while (1) {
   ```
   - Memulai loop utama untuk menerima dan menangani koneksi dari klien.

10. **Pemantauan Koneksi Menggunakan `select`**:
   ```c
   FD_ZERO(&readfds); // Kosongkan set file descriptor
   FD_SET(server_fd, &readfds); // Tambahkan server socket ke set

   int max_fd = server_fd;

   // Tambahkan klien socket yang valid ke dalam set
   for (int i = 0; i < num_clients; i++) {
       int client_socket = client_sockets[i];
       if (client_socket > 0) {
           FD_SET(client_socket, &readfds);
           if (client_socket > max_fd) {
               max_fd = client_socket;
           }
       }
   }
   ```
   - Mengosongkan set file descriptor dan menambahkan socket server dan klien yang valid ke dalam set.

11. **Menggunakan `select`**:
    ```c
    int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);

    if (activity < 0) {
        perror("select");
        exit(EXIT_FAILURE);
    }
    ```
    - Menggunakan fungsi `select` untuk memantau aktivitas pada koneksi socket.

12. **Cek Koneksi Baru**:
    ```c
    if (FD_ISSET(server_fd, &readfds)) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // Tambahkan soket klien ke daftar
        if (num_clients < MAX_CLIENTS) {
            client_sockets[num_clients++] = new_socket;
            printf("Client %d connected\n", num_clients);
        } else {
            printf("Maximum number of clients reached. Closing new connection.\n");
            close(new_socket);
            exit(1);
        }
    }
    ```
    - Jika ada koneksi baru, terima koneksi menggunakan `accept`. Jika jumlah klien belum mencapai maksimum, tambahkan soket klien baru ke daftar.

13. **Cek Data dari Klien**:
    ```c
    for (int i = 0; i < num_clients; i++) {
        int client_socket = client_sockets[i];
        if (FD_ISSET(client_socket, &readfds)) {
            valread = read(client_socket, buffer, sizeof(buffer));
            if (valread <= 0) {
                // Koneksi terputus
                printf("Client %d disconnected\n", i + 1);
                close(client_socket);
                num_clients--;
                client_sockets[i] = 0;
            } else {
                printf("Message from client %d: %s\n", i + 1, buffer);
                memset(buffer, 0, sizeof(buffer)); // Reset buffer
            }
        }
    }
    ```
    - Jika ada data dari klien, baca data tersebut. Jika jumlah byte yang dibaca kurang dari atau sama dengan 0, maka koneksi terputus dan klien harus ditutup.

 Jika tidak, tampilkan pesan dari klien dan reset buffer.

14. **Kembalikan 0**:
    ```c
    return 0;
    ```
    - Akhir dari fungsi `main`. Program berakhir dengan status keluaran 0.

## Output

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/ca14164a-228f-40c7-9e2c-57324a322eed)

Ketika client yang tersambung lebih dari 5

![image](https://github.com/J0see1/J0see1.github.io/assets/134209563/ce244593-0d31-4511-b0de-79e9d5ef95d6)

Semua client bisa mengirim pesan dan setelahnya langsung terdisconnect 

## Kendala 

Ketika Client yang melakukan pengiriman pesan adalah client pertama, maka client-client setelahnya tidak dapat melakukan pengiriman pesan dan program akan berhenti. Tapi ketika client yang mengirim adalah client paling terakhir, maka pesan akan terkirim dan program akan berjalan dengan normal.
