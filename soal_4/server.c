#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

fd_set readfds; // Set file descriptor untuk pemantauan

#define PORT 8080
#define MAX_CLIENTS 5

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
    int client_sockets[MAX_CLIENTS] = {0}; // Menyimpan file descriptor untuk klien
    
    // Membuat soket server
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    
    // Set opsi soket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    
    // Mengeksekusi bind
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Memulai mendengarkan
    if (listen(server_fd, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    int num_clients = 0;

    while (1) {
    FD_ZERO(&readfds); // Kosongkan set file descriptor
    FD_SET(server_fd, &readfds); // Tambahkan server socket ke set

    int max_fd = server_fd;

    // Tambahkan klien socket yang valid ke dalam set
    for (int i = 0; i < num_clients; i++) {
        int client_socket = client_sockets[i];
        if (client_socket > 0) {
            FD_SET(client_socket, &readfds);
            if (client_socket > max_fd) {
                max_fd = client_socket;
            }
        }
    }

    // Gunakan select untuk memantau koneksi
    int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);

    if (activity < 0) {
        perror("select");
        exit(EXIT_FAILURE);
    }

    // Cek server socket untuk koneksi baru
    if (FD_ISSET(server_fd, &readfds)) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // Tambahkan soket klien ke daftar
        if (num_clients < MAX_CLIENTS) {
            client_sockets[num_clients++] = new_socket;
            printf("Client %d connected\n", num_clients);
        } else {
            printf("Maximum number of clients reached. Closing new connection.\n");
            close(new_socket);
            exit(1);
        }
    }

    // Cek klien socket untuk data masuk
    for (int i = 0; i < num_clients; i++) {
        int client_socket = client_sockets[i];
        if (FD_ISSET(client_socket, &readfds)) {
            valread = read(client_socket, buffer, sizeof(buffer));
            if (valread <= 0) {
                // Koneksi terputus
                printf("Client %d disconnected\n", i + 1);
                close(client_socket);
                num_clients--;
                client_sockets[i] = 0;
            } else {
                printf("Message from client %d: %s\n", i + 1, buffer);
                memset(buffer, 0, sizeof(buffer)); // Reset buffer
            }
        }
    }
}

    return 0;
}
