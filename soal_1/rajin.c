#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long faktorial(int n) {
   if (n == 0) return 1;
   unsigned long long hasil = 1;
   for (int i = 1; i <= n; i++) {
       hasil *= i;
   }
   return hasil;
}
int main() {
   // Key yang digunakan harus sama dengan key yang digunakan dalam belajar.c
   key_t key = ftok("belajar.c", 'C');
   if (key == -1) {
       perror("ftok");
       exit(1);
   }
   int shmid = shmget(key, sizeof(int[3][3]), 0666);
   if (shmid == -1) {
       perror("shmget");
       exit(1);
   }
   // Attach shared memory ke alamat program
   int (*sharedMatriks)[3] = shmat(shmid, 0, 0);
   if (sharedMatriks == (int (*)[3]) -1) {
       perror("shmat");
       exit(1);
   }
   // Transpose matriks yang dibagikan melalui shared memory
   int transpose[3][3];
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           transpose[i][j] = sharedMatriks[j][i];
       }
   }
   printf("Matriks Transpose:\n");
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           printf("%d ", transpose[i][j]);
       }
       printf("\n");
   }
   // Hitung faktorial dalam format matriks
   unsigned long long hasilMatriks[3][3];
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           hasilMatriks[i][j] = faktorial(transpose[i][j]);
       }
   }
   printf("Matriks Hasil Faktorial tanpa thread:\n");
   for (int i = 0; i < 3; i++) {
       for (int j = 0; j < 3; j++) {
           printf("%llu ", hasilMatriks[i][j]);
       }
       printf("\n");
   }
   // Detach shared memory dari alamat program
   shmdt(sharedMatriks);
   return 0;
}
