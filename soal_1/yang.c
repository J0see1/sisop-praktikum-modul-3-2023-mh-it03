#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#define MATRIKS_SIZE 3
// Struktur untuk mengirimkan data ke thread
struct DataThread {
   int baris;
   int kolom;
   int* Matriks;
};
// Fungsi untuk menghitung faktorial
unsigned long long faktorial(int n) {
   if (n == 0) return 1;
   return n * faktorial(n - 1);
}
// Fungsi thread untuk menghitung faktorial
void* menghitungfaktorial(void* arg) {
   struct DataThread* data = (struct DataThread*)arg;
   int baris = data->baris;
   int kolom = data->kolom;
   int* Matriks = data->Matriks;
   // Hitung faktorial dan simpan di dalam matriks
   Matriks[baris * MATRIKS_SIZE + kolom] = faktorial(Matriks[baris * MATRIKS_SIZE + kolom]);

   pthread_exit(NULL);
}
int main() {
   // Key yang digunakan harus sama dengan key yang digunakan dalam belajar.c
   key_t key = ftok("belajar.c", 'C');
   if (key == -1) {
       perror("ftok");
       exit(1);
   }
   int shmid = shmget(key, sizeof(int[MATRIKS_SIZE][MATRIKS_SIZE]), 0666);
   if (shmid == -1) {
       perror("shmget");
       exit(1);
   }
   // Attach shared memory ke alamat program
   int (*sharedMatriks)[MATRIKS_SIZE] = shmat(shmid, 0, 0);
   if (sharedMatriks == (int (*)[MATRIKS_SIZE]) -1) {
       perror("shmat");
       exit(1);
   }
   // Transpose matriks yang dibagikan melalui shared memory
   int transpose[MATRIKS_SIZE][MATRIKS_SIZE];
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           transpose[i][j] = sharedMatriks[j][i];
       }
   }
   printf("Matriks Transpose:\n");
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           printf("%d ", transpose[i][j]);
       }
       printf("\n");
   }
   // Buat thread untuk menghitung faktorial
   pthread_t threads[MATRIKS_SIZE][MATRIKS_SIZE];
   struct DataThread DataThread[MATRIKS_SIZE][MATRIKS_SIZE];
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           DataThread[i][j].baris = i;
           DataThread[i][j].kolom = j;
           DataThread[i][j].Matriks = (int*)transpose;

           pthread_create(&threads[i][j], NULL, calculatefaktorial, &DataThread[i][j]);
       }
   }
   // Tunggu hingga semua thread selesai
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           pthread_join(threads[i][j], NULL);
       }
   }
   printf("Matriks Hasil Faktorial:\n");
   for (int i = 0; i < MATRIKS_SIZE; i++) {
       for (int j = 0; j < MATRIKS_SIZE; j++) {
           printf("%llu ", (unsigned long long)transpose[i][j]);
       }
       printf("\n");
   }
   // Detach shared memory dari alamat program
   shmdt(sharedMatriks);

   return 0;
}
