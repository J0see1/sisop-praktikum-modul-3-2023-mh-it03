#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int main() {
    // Menentukan ukuran matriks
    int nomor_kelompok = 3; 
    int baris1 = nomor_kelompok;
    int kolom1 = 2;
    int baris2 = 2;
    int kolom2 = nomor_kelompok;

    // Inisialisasi seed untuk angka acak
    srand(time(NULL));

    // Membuat matriks pertama dengan angka acak antara 1 hingga 4
    int matriks1[baris1][kolom1];
    for (int l = 0; l < baris1; l++) {
        for (int k = 0; k < kolom1; k++) {
            matriks1[l][k] = rand() % 4 + 1;
        }
    }
    // Membuat matriks kedua dengan angka acak antara 1 hingga 5
    int matriks2[baris2][kolom2];
    for (int l = 0; l < baris2; l++) {
        for (int k = 0; k < kolom2; k++) {
            matriks2[l][k] = rand() % 5 + 1;
        }
    }
    printf("Matriks Pertama:\n");
    for (int l = 0; l < baris1; l++) {
        for (int k = 0; k < kolom1; k++) {
            printf("%d ", matriks1[l][k]);
        }
        printf("\n");
    }
    printf("\nMatriks Kedua:\n");
    for (int l = 0; l < baris2; l++) {
        for (int k = 0; k < kolom2; k++) {
            printf("%d ", matriks2[l][k]);
        }
        printf("\n");
    }
    // Melakukan perkalian matriks dan menampilkan hasil awal
    if (kolom1 != baris2) {
        printf("\nPerkalian matriks tidak dapat dilakukan.\n");
    } else {
        int hasil[baris1][kolom2];
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                hasil[l][k] = 0;
                for (int t = 0; t < kolom1; t++) {
                    hasil[l][k] += matriks1[l][t] * matriks2[t][k];
                }
            }
        }
        printf("\nHasil Perkalian Matriks:\n");
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                printf("%d ", hasil[l][k]);
            }
            printf("\n");
        }
        // Mengurangi 1 pada hasil perkalian
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                hasil[l][k] -= 1;
            }
        }
        printf("\nHasil Perkalian Matriks - 1:\n");
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                printf("%d ", hasil[l][k]);
            }
            printf("\n");
        }
        // Membuat shared memory segment
        key_t key = ftok("belajar.c", 'C');
        if (key == -1) {
            perror("ftok");
            exit(1);
        }
        int shmid = shmget(key, sizeof(hasil), 0666 | IPC_CREAT);
        if (shmid == -1) {
            perror("shmget");
            exit(1);
        }
        // Melampirkan shared memory ke alamat program
        int (*sharedMatriks)[kolom2] = shmat(shmid, 0, 0);
        if (sharedMatriks == (int (*)[kolom2]) -1) {
            perror("shmat");
            exit(1);
        }
        // Menyalin hasil perkalian matriks ke shared memory
        for (int l = 0; l < baris1; l++) {
            for (int k = 0; k < kolom2; k++) {
                sharedMatriks[l][k] = hasil[l][k];
            }
        }
        // Detach shared memory dari alamat program
        shmdt(sharedMatriks);
    }
    return 0;
}
