#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

struct message {
    long msg_type;
    char msg_text[256];
};

// Fungsi decrypt base64
char *decryptBase64(const char *input) {
    BIO *b64, *bio;
    char *buffer = NULL;
    size_t length;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    length = strlen(input) * 3 / 4;

    buffer = (char *)malloc(length + 1);
    if (buffer == NULL) {
        BIO_free_all(b64);
        return NULL;
    }
    memset(buffer, 0, length + 1);

    bio = BIO_new_mem_buf((void *)input, -1);
    bio = BIO_push(b64, bio);
    BIO_read(bio, buffer, strlen(input));
    BIO_free_all(bio);
    return buffer;
}

int main() {
    int msgid;
    struct message msg;
    int authenticated = 0;

    while(1){
       key_t key = ftok("sender", 'R');
        if (key == -1) {
            perror("ftok");
            exit(1);
        }

        msgid = msgget(key, 0666 | IPC_CREAT);
        if (msgid == -1) {
            perror("msgget");
            exit(1);
        }

        if (msgrcv(msgid, &msg, sizeof(msg.msg_text), 1, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }

        printf("Pesan yang diterima: %s\n", msg.msg_text);

        if (strcmp(msg.msg_text, "CREDS") == 0) {
            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];
            while (fgets(line, sizeof(line), file) != NULL) {
                char *username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);
                printf("Username: %s, Password: %s\n", username, decoded_password);
                free(decoded_password);
            }
            fclose(file);

        } else if (strncmp(msg.msg_text, "AUTH:", 5) == 0) {
            char *username = strtok(msg.msg_text + 6, " ");
            char *password = strtok(NULL, " ");

            FILE *file = fopen("users/users.txt", "r");
            if (file == NULL) {
                perror("File tidak ditemukan");
                exit(1);
            }

            char line[100];

            while (fgets(line, sizeof(line), file) != NULL) {
                char *file_username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");
                char *decoded_password = decryptBase64(encoded_password);

                if (strcmp(username, file_username) == 0 && strcmp(password, decoded_password) == 0) {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                free(decoded_password);
            }
            fclose(file);

        } else if (authenticated == 1 && strncmp(msg.msg_text, "TRANSFER:", 9) == 0) {
            char *filename = strtok(msg.msg_text + 10, " ");
            char sender_filepath[100];
            char receiver_filepath[100];

            sprintf(sender_filepath, "Sender/%s", filename);
            sprintf(receiver_filepath, "Receiver/%s", filename);

            // cek keberadaan file
            if (access(receiver_filepath, F_OK) != -1) {
                printf("File '%s' already exists in Receiver directory. Skipping transfer.\n", filename);
                break;
            } else {
                if (rename(sender_filepath, receiver_filepath) != 0) {
                    perror("Gagal memindahkan file");
                    exit(1);
                }

                FILE *file = fopen(receiver_filepath, "rb");
                fseek(file, 0, SEEK_END);
                long size = ftell(file);
                fclose(file);

                float size_kb = size / 1024.0;

                printf("File '%s' berhasil dipindahkan dari Sender ke Receiver. Ukuran: %.2f KB\n", filename, size_kb);
            }
                } else if (!authenticated) {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    break;
                } else {
            printf("UNKNOWN COMMAND: %s\n", msg.msg_text);
        }
    }
    return 0;
}