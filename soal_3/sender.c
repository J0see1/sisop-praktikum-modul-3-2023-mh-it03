#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define MAX 100

int status;

// structure for message queue
struct message {
    long msg_type;
    char msg_text[256];
};

void downloadFile() {
    pid_t wget = fork();
    if (wget == 0){
        execl("/usr/bin/wget", "wget", "-O", "users.zip", "https://drive.google.com/u/0/uc?id=1CrERpikZwuxgAwDvrhRnB2VyAYtT2SIf&export=download", NULL );
        exit(EXIT_SUCCESS);
    }  waitpid(wget, &status, 0);
}

void extractZip(){
    pid_t unzip = fork();
    if (unzip == 0) {
        execl("/usr/bin/unzip", "unzip", "-o", "users.zip",  NULL);
        perror("execl");
        exit(EXIT_FAILURE); 
    }
    else if (unzip > 0) {

        waitpid(unzip, &status, 0); 
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            remove("users.zip"); 
        } else {
            printf("Unzip failed.\n");
        }
    }
    else {
        perror("fork");
    }
    waitpid(unzip, &status, 0);
}

void makeDirectory() {
    pid_t mkdirSender, mkdirReceiver;

    mkdirSender = fork();

    if (mkdirSender == 0) {
        int create = mkdir("Sender", 0777); 
        if (create != 0) {
            perror("Error creating directory");
        } else {
            printf("Directory Sender created successfully.\n");
        }
        exit(EXIT_SUCCESS);
    } else if (mkdirSender > 0) {
        mkdirReceiver = fork();
        if (mkdirReceiver == 0) {
            int create = mkdir("Receiver", 0777); 
            if (create != 0) {
                perror("Error creating directory");
            } else {
                printf("Directory Receiver created successfully.\n");
            }
            exit(EXIT_SUCCESS);
        } else if (mkdirReceiver < 0) {
            perror("Error forking child process 2");
        }
    } else {
        perror("Error forking child process 1");
    }
}

int main() {
    makeDirectory();
    downloadFile();
    extractZip();

    int msgid;
    struct message msg;

	// ftok to generate unique key
    key_t key = ftok("sender", 'R');
    if (key == -1) {
        perror("ftok");
        exit(1);
    }

	// msgget creates a message queue
	// and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }
 
    while(1){
        
    printf("Masukkan pesan Anda: ");
    fgets(msg.msg_text, MAX, stdin);
    msg.msg_text[strcspn(msg.msg_text, "\n")] = 0;  

    msg.msg_type = 1;

	// msgsnd to send message
    if (msgsnd(msgid, &msg, strlen(msg.msg_text) + 1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

	// display the message
    printf("Pesan telah terkirim ke receiver.\n");
    }
    return 0;
}