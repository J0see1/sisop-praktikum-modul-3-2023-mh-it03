#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>

#define MAX_WORD_LEN 100
#define MAX_LINE_LEN 1000

void removeNonAlphabetic(char *str) {
    int i, j = 0;
    for (i = 0; str[i]; i++) {
        if (isalpha(str[i])) {
            str[j++] = str[i];
        }
    }
    str[j] = '\0';
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        fprintf(stderr, "Usage: %s -kata or %s -huruf\n", argv[0], argv[0]);
        exit(EXIT_FAILURE);
    }

    // Baca file lirik
    FILE *inputFile = fopen("lirik.txt", "r");
    if (inputFile == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    // Fork process
    pid_t pid;
    int pipefd[2];
    if (pipe(pipefd) == -1) {
        perror("pipe failed");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    }

    if (pid > 0) { // Parent process
        close(pipefd[1]); // Close write end of the pipe

        // Read from the pipe and log the result
        char buffer[MAX_LINE_LEN];
        read(pipefd[0], buffer, sizeof(buffer));

        // buat file log
        FILE *logFile = fopen("frekuensi.log", "a");
        fprintf(logFile, "[%s] [%s] %s", "current_date", argv[1] + 1, buffer);
        fclose(logFile);

        close(pipefd[0]); // Close read end of the pipe
        wait(NULL);
    } else { // Child process
        close(pipefd[0]); // Close read end of the pipe

        // memproses file dan menghitung frekuensi
        char line[MAX_LINE_LEN];
        char word[MAX_WORD_LEN];

        int count = 0;

        if (strcmp(argv[1], "-kata") == 0) {
            // menghitung frekuensi kata
            while (fscanf(inputFile, "%s", word) == 1) {
                removeNonAlphabetic(word);
                if (strlen(word) > 0) {
                    if (strcasecmp(word, "kata_yang_dicari") == 0) {
                        count++;
                    }
                }
            }
        } else if (strcmp(argv[1], "-huruf") == 0) {
            // menghitung frekuensi huruf
            while (fgets(line, sizeof(line), inputFile) != NULL) {
                int i;
                for (i = 0; line[i]; i++) {
    			if (isalpha(line[i])) {
                        if (tolower(line[i]) == 'a') {
                            count++;
                        }
                    }
                }
            }
        }

        // write hasil dari pipe
        char result[MAX_LINE_LEN];
        sprintf(result, "%s 'kata_atau_huruf' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", argv[1], count);
        write(pipefd[1], result, strlen(result) + 1);

        close(pipefd[1]); // Close write end of the pipe
        fclose(inputFile);
        exit(EXIT_SUCCESS);
    }

    return 0;
}

